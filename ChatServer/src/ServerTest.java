import javax.swing.JFrame;

public class ServerTest {
    public static void main(String[] args){
        Server user = new Server();
        user.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        user.startRunning();
    }
}